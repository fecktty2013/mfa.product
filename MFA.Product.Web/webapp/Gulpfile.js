const gulp = require('gulp');

function build() {
    return gulp.src("dist/**")
      .pipe(gulp.dest('dist/product/'));
}

exports.build = build;
