import { Component, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { NzIconService } from 'ng-zorro-antd';

@Component({
    selector: 'video-component',
    templateUrl: './video.component.html',
    styleUrls: ['./video.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class VideoComponent {
    constructor(private fb: FormBuilder, private iconService: NzIconService) {
        iconService.changeAssetsSource("/product");
    }

    tagOptions: Array<{ label: string; value: string }> = [];
    categoryOptions: Array<{ label: string; value: string }> = [];
    statusOptions: Array<{ label: string; value: string }> = [];

    myForm: FormGroup;

    videoData = [
        {id: 1, title: 'Test Video A', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 2, title: 'Test Video B', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 3, title: 'Test Video C', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 4, title: 'Test Video D', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 5, title: 'Test Video E', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 6, title: 'Test Video F', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 7, title: 'Test Video G', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
        {id: 8, title: 'Test Video H', publishTime: '2020-04-06T03:02:05.424Z', state: 'Active', recommendWeight: 50, watchRevenue: 120, downloadRevenue: 50, earnings: 33, comments: 5 },
    ];

    ngOnInit(): void {
        const children: Array<{ label: string; value: string }> = [];
        children.push({ label: 'Fun', value: 'fun' });
        children.push({ label: 'Music', value: 'music' });
        children.push({ label: 'Science', value: 'science' });
        this.tagOptions = children;

        const categoryChildren: Array<{ label: string; value: string }> = [];
        categoryChildren.push({ label: 'Art', value: 'art' });
        categoryChildren.push({ label: 'Education', value: 'education' });
        categoryChildren.push({ label: 'Entertainment', value: 'entertainment' });
        this.categoryOptions = categoryChildren;

        const statusChildren: Array<{ label: string; value: string }> = [];
        statusChildren.push({ label: 'All', value: 'all' });
        statusChildren.push({ label: 'Active', value: 'active' });
        statusChildren.push({ label: 'Inactive', value: 'inactive' });
        this.statusOptions = statusChildren;

        this.myForm = this.fb.group({});
        this.myForm.addControl('sortOption', new FormControl("weight"));
        this.myForm.addControl('videoid', new FormControl());
        this.myForm.addControl('videoTitle', new FormControl());
        this.myForm.addControl('selectTags', new FormControl());
        this.myForm.addControl('selectStatus', new FormControl());
        this.myForm.addControl('datePicker', new FormControl());
    }
}
